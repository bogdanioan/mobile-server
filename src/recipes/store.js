import dataStore from 'nedb-promise';

export class RecipeStore {
    constructor({ filename, autoload }) {
        this.store = dataStore({ filename, autoload });
    }

    async find(props) {
        if (props._id) {
            return this.store.findOne(props);
        }
        return this.store.find({ userEmail: props.userEmail });
    }

    async insert(recipe) {
        recipe.added = new Date();
        return this.store.insert(recipe);
    }

    async update(props, recipe) {
        let dbRecipe = await this.find({ _id: recipe.uuid });
        if (!dbRecipe) throw new Error('404');
        delete recipe.uuid;
        recipe.added = new Date();
        return this.store.update(props, recipe);
    }

    async remove(props) {
        return this.store.remove(props);
    }
}

export default new RecipeStore({ filename: './db/recipes.json', autoload: true });