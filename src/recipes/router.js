import Router from 'koa-router';
import RecipeStore from './store';

export const router = new Router();

router.get('/', async (ctx) => {
    const response = ctx.response;
    const url = ctx.request.url;
    const paramsUrl = url.split('?')[1];
    const params = paramsUrl.split('&');
    const page = params[0].split('=')[1];
    const count = params[1].split("=")[1];
    let recipes = await RecipeStore.find({ userEmail: ctx.state.user.email });
    recipes = recipes.sort((a,b)=> -(a.added.getTime() - b.added.getTime()));
    recipes = recipes.slice(page*count, (page+1)*count);
    response.body = recipes;
    response.status = 200; // ok
});

// router.get('/:id', async (ctx) => {
//     const response = ctx.response;
//     const _id = ctx.params.id;
//     const recipe = await RecipeStore.find({ _id, userId: ctx.state.user._id });
//     response.body = recipe;
//     response.status = 200; // ok
//     response.lastModified = new Date(recipe.updated);
//     response.etag = recipe.version;
// });

router.post('/', async (ctx) => {
    const recipe = ctx.request.body;
    const response = ctx.response;
    try {
        recipe.userEmail = ctx.state.user.email;
        response.body = await RecipeStore.insert(recipe);
        response.status = 200; // ok
    } catch (err) {
        response.body = [{ issue: err.message, type: 'Error' }];
        response.status = 400; // not ok
    }
});

router.put('/:id', async (ctx) => {
    const recipe = ctx.request.body;
    const id = ctx.params.id;
    const response = ctx.response;
    recipe.userEmail = ctx.state.user.email;
    try {
        await RecipeStore.update({ _id: id }, recipe);
        recipe._id = id;
        response.body = recipe;
        response.status = 200; // ok
    } catch (err) {
        response.body = [{ issue: err.message, type: 'Error' }];
        response.status = parseInt(err.message) || 400; // not ok
    }
});


router.del('/:id', async (ctx) => {
    await RecipeStore.remove({ _id: ctx.params.id });
    ctx.response.status = 200; // ok
});
