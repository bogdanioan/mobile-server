import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import Router from 'koa-router';
import jwt from 'koa-jwt';
import cors from 'koa-cors';
import { exceptionHandler, timingLogger, jwtConfig } from "./utils";
import { router as recipeRouter } from './recipes';
import { router as authRouter } from './auth';


const app = new Koa();
const server = require('http').createServer(app.callback());
app.use(exceptionHandler);
app.use(timingLogger);
app.use(bodyParser());
app.use(cors());

const prefix = '/api';

// public
const publicApiRouter = new Router({ prefix });
publicApiRouter
  .use('/auth', authRouter.routes());
app
  .use(publicApiRouter.routes())
  .use(publicApiRouter.allowedMethods());

app.use(jwt(jwtConfig));

const apiRouter = new Router({ prefix });
apiRouter
  .use('/recipes', recipeRouter.routes());
app
  .use(apiRouter.routes())
  .use(apiRouter.allowedMethods());

server.listen(3000);
console.log("Listening on port 3000");
