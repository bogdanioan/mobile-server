import Router from 'koa-router';
import jwt from 'jsonwebtoken';
import userStore from './store';
import { jwtConfig } from '../utils';

export const router = new Router();

const createToken = (user) => {
    return jwt.sign({ email: user.email, _id: user._id }, jwtConfig.secret, { expiresIn: 60 * 60 * 60 });
};

router.post('/login', async (ctx) => {
    const credentials = ctx.request.body;
    const response = ctx.response;
    const user = await userStore.findOne(credentials);
    if (user) {
        response.body = { token: createToken(user) };
        response.status = 200; // ok
    } else {
        response.body = { issue: [{ error: 'Invalid credentials' }] };
        response.status = 400; // bad request
    }
});